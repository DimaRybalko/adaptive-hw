const activeBtn = document.querySelector(".first-icon");
const disableBtn = document.querySelector(".second-icon");
const list = document.querySelector(".burger-nav__list");

activeBtn.addEventListener("click", () => {
  list.classList.toggle("active");
  disableBtn.classList.toggle("active");
});

disableBtn.addEventListener("click", () => {
  list.classList.remove("active");
  disableBtn.classList.remove("active");
});
